#!/bin/bash

bold=`tput bold`
normal=`tput sgr0`
while true
do
    for i in 1 2 3 
    do
        node=${1:-h1-$i}
        out=`sudo python run.py --node $node --cmd "curl -s 18.0.2.1 -w "@curl-format.txt" -o received-duck.jpg"`
        date=`date`
        echo $date -- $bold$out$normal
        echo $out >> curl-stats.txt
        sleep 2
    done
done