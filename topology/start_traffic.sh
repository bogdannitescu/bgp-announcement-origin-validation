#!/bin/bash

rm scenario-stats/eval-detection-timestamps.txt
date=`date`
echo "Experiment Start Time: $date" >> scenario-stats/eval-detection-timestamps.txt
rm curl-stats.txt
/bin/bash website-AS8-requests-ducks-from-AS1.sh &
pid[0]=$!

/bin/bash website-AS1-requests-ducks-from-AS8.sh &
pid[1]=$!

trap "kill ${pid[0]} ${pid[1]}; exit 1" INT
wait