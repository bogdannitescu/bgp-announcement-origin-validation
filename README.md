# Abstract

This project proposes a new concept to combat BGP Hijacking attacks. We devise an approach for any individual network to avoid that its traffic is diverted because of a BGP hijack. The novelty of our solution relates to the approach of using Software Defined Networks to enhance the security of the BGP protocol in a pro-active way by exchanging validation packets through an encoded mechanism based on a secret that is naturally shared between the participants: the state of the TCP connections.

We have identified a common issue of the existing state-of-the-art solutions for the BGP Hijacking attacks: they propose a reactive approach. In this project, our goal is to develop a pro-active system that can prevent hijack attacks from happening and to encourage the community to conduct future research based on the validation concept that we propose.

This project aims to present a mechanism that is an early stepping stone towards an internet where path validation between Autonomous Systems can be achieved. Our system is designed to be implemented as a subscription based service where any Autonomous System in the Internet can benefit to have a Sentinel/Protector Autonomous System that protects its network from traffic hijacks.

The challenging aspects of our goal is to develop a system that can perform the validation process in a fast and reliable way, without affecting the legitimate traffic in the Internet and preventing the attacker to identify and block the validation packets. 

In this project, we implement the proposed solution in a custom defined virtual network. We perform evaluation experiments of the solution in the virtual network in multiple scenarios designed to highlight the strengths and weaknesses of the system.

Our results show that the proposed system can detect and prevent the hijack attack in less challenging conditions but also in more challenging scenarios where the network can be congested or the attacker performs a more advanced hijack that tries to block the validation process.


## Installation

### Mininet
[Mininet](http://mininet.org/) Virtual Network is required to run the emulated topology in which we implement the validation system. 
The best way to get Mininet is to download the Mininet VM installation from http://www.scs.stanford.edu/~jvimal/mininet-sigcomm14/mininet-tutorial-vm-64bit.zip . 
For VM set up instructions, follow: http://mininet.org/vm-setup-notes/ . 
This implementation is not guaranteed to work with another version of Mininet. The above VM has most of the requirements already installed and it is the same one on which we have implemented this system.

### Requirements
In the Mininet VM, clone this repository and install the requirements:
```bash
% git clone https://bogdannitescu@bitbucket.org/bogdannitescu/bgp-announcement-origin-validation.git
% cd bgp-announcement-origin-validation
% pip install -r requirements.txt -v
% ./topology/install.sh
```

### Ryu
[Ryu](https://osrg.github.io/ryu/) SDN Framework is used to define and start the Controllers that allow us to perform the validation process.

Clone the Ryu framework and install the framework:

```bash
% cd topology
% git clone git://github.com/osrg/ryu.git
% python ./ryu/setup.py install 
```

## Usage
This version of the project is a "clean" version - it does not include the necessary code to simulate a bgp hijack. 
The following usage instructions do not include a demonstration of the system in hijack conditions. 

Additional files and instructions required to launch a bgp hijack are available upon request. 

If you are in ```bgp-announcement-origin-validation/topology```, you can follow the next usage instructions to start the virtual network and populate it with traffic. 

### Controllers
Before we start the virtual network, we first initialise each Ryu Controller.

To start the Sentinel Controller for AS8, open a *new terminal* and run:
```bash
% ./start_R8_sentinel.sh
```

To start the Monitor Controller for AS1, run in another *new terminal*:
```bash
% ./start_R1_controller_tcp_monitor.sh
```

### Start Mininet
The virtual network can be started in a new terminal with:
```bash
% sudo python bgp-multiple-controllers.py
```

### Network Traffic
Wait about 30-60 seconds for the network routes to converge. When the network converges, every node should be reachable through one ore more paths.
To start the traffic between hosts in the network, run in a *new terminal*:
```bash
% ./start_traffic.sh
```

Traffic will cause the Controllers to output information:

1. The Sentinel Controller outputs the updated state of the BGP Routing Table after every BGP announcement that results in an update of the table. If there is a hijack attempt to any of the prefixes that the Sentinel owns, it shows that the validation process is started and it outputs updates about the validation process.
2. The Victim Controller outputs the state of the Out-of-Order frequency dictionary after each recorded out-of-order tcp packet. If the Controller detects a signal about a BGP Hijack sent by the Sentinel, the Victim Controller outputs information about the outgoing validation packets.

To stop the network traffic and all the sub-processes of the Mininet hosts that run ```curl``` requests to any existing webservers, run:
```bash
% ./stop_traffic.sh
```
To stop the Mininet, ```Ctrl-D``` in the Mininet CLI.

Top stop each individual controller, ```Ctrl-C``` in the terminals where the controllers have been started.


## License
[MIT](https://choosealicense.com/licenses/mit/)