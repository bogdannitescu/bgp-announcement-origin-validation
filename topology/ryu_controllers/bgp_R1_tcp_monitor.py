# Copyright (C) 2011 Nippon Telegraph and Telephone Corporation.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
# implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from ryu.base import app_manager
from ryu.controller import ofp_event
from ryu.controller.handler import CONFIG_DISPATCHER, MAIN_DISPATCHER
from ryu.controller.handler import set_ev_cls
from ryu.ofproto import ofproto_v1_3
from ryu.lib.packet import packet
from ryu.lib.packet import ethernet
from ryu.lib.packet import ipv4
from ryu.lib.packet import icmp
from ryu.lib.packet import ether_types
from ryu.lib.packet import tcp
from ryu.lib.packet import bgp

from timeit import default_timer as timer
from topoutils.topology_utils import Monitor_TCP_Flow, Signal_Detector, Shift_Rule

import time 

import array
import pprint

import json

import os
import sys

class SimpleSwitch13(app_manager.RyuApp):
    OFP_VERSIONS = [ofproto_v1_3.OFP_VERSION]

    def __init__(self, *args, **kwargs):
        super(SimpleSwitch13, self).__init__(*args, **kwargs)
        self.mac_to_port = {}
        self.TCP_flows = []
        self.OoO_Freq_Dict = {}
        self.sentinel_prefix = "18.0.0.0"
        self.TCP_Shifting_Rules = []
        self.Signal_Detector = Signal_Detector(SIGNAL_THRESHOLD = 10)
        self.target_number_of_shifted_packets = 1
        self.shifting_rule_deadline_ts = {}
        self.answer_signal_duration = 900 # (seconds)

    @set_ev_cls(ofp_event.EventOFPSwitchFeatures, CONFIG_DISPATCHER)
    def switch_features_handler(self, ev):
        datapath = ev.msg.datapath
        ofproto = datapath.ofproto
        parser = datapath.ofproto_parser

        # install table-miss flow entry
        #
        # We specify NO BUFFER to max_len of the output action due to
        # OVS bug. At this moment, if we specify a lesser number, e.g.,
        # 128, OVS will send Packet-In with invalid buffer_id and
        # truncated packet data. In that case, we cannot output packets
        # correctly.  The bug has been fixed in OVS v2.1.0.
        match = parser.OFPMatch()
        actions = [parser.OFPActionOutput(ofproto.OFPP_CONTROLLER,
                                          ofproto.OFPCML_NO_BUFFER)]
        self.add_flow(datapath, 0, match, actions)

    def add_flow(self, datapath, priority, match, actions, buffer_id=None):
        ofproto = datapath.ofproto
        parser = datapath.ofproto_parser

        inst = [parser.OFPInstructionActions(ofproto.OFPIT_APPLY_ACTIONS,
                                             actions)]
        if buffer_id:
            mod = parser.OFPFlowMod(datapath=datapath, buffer_id=buffer_id,
                                    priority=priority, match=match,
                                    instructions=inst)
        else:
            mod = parser.OFPFlowMod(datapath=datapath, priority=priority,
                                    match=match, instructions=inst)
        datapath.send_msg(mod)
    
    def check_for_signal(self):
        detector = self.Signal_Detector
        freq_dict = self.OoO_Freq_Dict
        amount = 0
        if detector.has_received_signal(freq_dict):
            amount = detector.get_outlier(freq_dict)
            # refresh OoO Frequency Dict
            self.OoO_Freq_Dict = {}
            # Add shifting rule
            self.shift_tcp_flow(flow_dest = self.sentinel_prefix, shift_amount = amount)
            return True, amount
        else:
            return False, amount
    
    def shift_tcp_flow(self, flow_dest, shift_amount):
        shift_rule = Shift_Rule(prefix = flow_dest, 
                                amount = shift_amount, 
                                counter = 0, 
                                target = self.target_number_of_shifted_packets)
        self.TCP_Shifting_Rules.append(shift_rule)
        # print("Shifted TCP Flows...")
        # for rule in self.TCP_Shifting_Rules:
        #     print(rule)

        # Update the timer when the shifting rule expires
        self.shifting_rule_deadline_ts[flow_dest] = int(time.time()) + self.answer_signal_duration
        return
    
    def check_expired_shifting_rule(self):
        this_time = int(time.time())
        # Reset shifting rules
        for prefix, expiration_ts in self.shifting_rule_deadline_ts.items():
            if this_time >= expiration_ts:
                # remove tcp rule
                for rule in self.TCP_Shifting_Rules:
                    if prefix == rule.prefix:
                        self.TCP_Shifting_Rules.remove(rule)
                # print("Expired rule...")
                # print("Remaning rules...")
                # for rule in self.TCP_Shifting_Rules:
                #     print(rule)
                del self.shifting_rule_deadline_ts[prefix]

    # Returns True if dst_ip is in the destination prefix
    # Eg. 11.0.0.1 in 11.0.0.0/8 TRUE
    #     12.0.0.1 in 11.0.0.0/8 FALSE
    def ip_in_prefix(self, dst_ip, dst_prefix):
        dst_ip = dst_ip.split('.')
        dst_prefix = dst_prefix.split('.')
        return dst_ip[0] == dst_prefix[0]
    
    def must_shift_payload(self, src_ip, dst_ip, shift_rule, dst_port, src_port):
        # destination prefix for which packets should be shifted
        shift_dst = shift_rule.prefix

        # if the packet destination IPv4 is not in the prefix
        if not self.ip_in_prefix(dst_ip = dst_ip, dst_prefix = shift_dst):
            return False
        
        # checks if enough packets have beenn shifted for this flow 
        if not shift_rule.can_shift_packet_for_flow(src_ip, dst_ip, dst_port, src_port):
            return False
        return True

    @set_ev_cls(ofp_event.EventOFPPacketIn, MAIN_DISPATCHER)
    def _packet_in_handler(self, ev):
        # If you hit this you might want to increase
        # the "miss_send_length" of your switch
        if ev.msg.msg_len < ev.msg.total_len:
            self.logger.debug("packet truncated: only %s of %s bytes",
                              ev.msg.msg_len, ev.msg.total_len)

        self.check_expired_shifting_rule()

        msg = ev.msg
        datapath = msg.datapath
        ofproto = datapath.ofproto
        parser = datapath.ofproto_parser
        in_port = msg.match['in_port']
        pkt = packet.Packet(msg.data)
        eth = pkt.get_protocols(ethernet.ethernet)[0]
        
        if eth.ethertype == ether_types.ETH_TYPE_LLDP:
            # ignore lldp packet
            return
        dst = eth.dst
        src = eth.src

        dpid = datapath.id
        self.mac_to_port.setdefault(dpid, {})

        # learn a mac address to avoid FLOOD next time.
        self.mac_to_port[dpid][src] = in_port

        if dst in self.mac_to_port[dpid]:
            out_port = self.mac_to_port[dpid][dst]
        else:
            out_port = ofproto.OFPP_FLOOD

        actions = [parser.OFPActionOutput(out_port)]

        data = None
        if msg.buffer_id == ofproto.OFP_NO_BUFFER:
            data = msg.data

        out = parser.OFPPacketOut(datapath=datapath, buffer_id=msg.buffer_id,
                                  in_port=in_port, actions=actions, data=data)
        if in_port > out_port:
            received_message = True
        else:
            received_message = False

        tcp_header = pkt.get_protocol(tcp.tcp)
        pkt_ipv4   = pkt.get_protocol(ipv4.ipv4)
        tcp_flow_already_monitored = False

        # Record out of order packes for each flow
        # Incoming packet has payload
        if tcp_header is not None and tcp_header.has_flags(0x001): # FIN Flag
            src_ip = pkt_ipv4.src
            dst_ip = pkt_ipv4.dst
            src_port = tcp_header.src_port
            dst_port = tcp_header.dst_port
            this_flow = Monitor_TCP_Flow(src_ip, dst_ip, src_port, dst_port)
            for flow in self.TCP_flows:
                # flow already recorded, update expected seq 
                if this_flow == flow:
                    self.TCP_flows.remove(flow)
                    break
        if tcp_header is not None and received_message:
            src_ip = pkt_ipv4.src
            dst_ip = pkt_ipv4.dst
            src_port = tcp_header.src_port
            dst_port = tcp_header.dst_port
            this_flow = Monitor_TCP_Flow(src_ip, dst_ip, src_port, dst_port)
            # flow already recorded
            if isinstance(pkt.protocols[-1], str):
                payload = pkt.protocols[-1]
                for flow in self.TCP_flows:
                    # flow already recorded, update expected seq 
                    if this_flow == flow:
                        seq_diff = flow.update_expected_seq(tcp_header.seq, len(payload))
                        # seq unexpected
                        if seq_diff != 0:
                            # do not record if there is an 
                            # ongoing answer for this signal already
                            record_seq = True
                            for shift_rule in self.TCP_Shifting_Rules:
                                if seq_diff == shift_rule.amount:
                                    record_seq =  False
                            if record_seq:
                                if seq_diff in self.OoO_Freq_Dict:
                                    self.OoO_Freq_Dict[seq_diff] += 1
                                else:
                                    self.OoO_Freq_Dict[seq_diff] = 1
                                print("Out-of-Order Frequency Dictionary")
                                pprint.pprint(self.OoO_Freq_Dict)
                                received_signal, signal = self.check_for_signal()
                                if received_signal:
                                    print("Answer signal with shift rule by amount: %d"%(signal))
                                    os.system("echo \"Detection Time: `date`\" >> scenario-stats/eval-detection-timestamps.txt")
                        break
            # new flow
            elif tcp_header.has_flags(tcp.TCP_SYN, tcp.TCP_ACK):
                this_flow.update_expected_seq(tcp_header.seq + 1, 0)
                self.TCP_flows.append(this_flow)
            
                
        # check if outgoing non-BGP message with payload must be shifted
        if tcp_header is not None and not (tcp_header.src_port is 179 or tcp_header.dst_port is 179):
            if received_message == True:
                for shift_rule in self.TCP_Shifting_Rules:
                    if tcp_header.has_flags(0x001): # FIN Flag
                        shift_rule.remove_flow(pkt_ipv4.src, pkt_ipv4.dst, tcp_header.dst_port, tcp_header.src_port)
            if received_message == False:
                for shift_rule in self.TCP_Shifting_Rules:
                    if not isinstance(pkt.protocols[-1], str):
                        continue
                    shift_amount = shift_rule.amount
                    if self.must_shift_payload(src_ip = pkt_ipv4.src,
                                            dst_ip = pkt_ipv4.dst, 
                                            shift_rule = shift_rule, 
                                            dst_port = tcp_header.dst_port,
                                            src_port = tcp_header.src_port):
                        print("Replying to Sentinel. Shifting TCP payload by %d"%(shift_amount))
                        new_pkt = packet.Packet()
                        for prt in pkt.protocols:
                            # ethernet
                            if isinstance(prt, ethernet.ethernet):
                                new_pkt.add_protocol(prt)
                            # ipv4
                            elif isinstance(prt, ipv4.ipv4):
                                ip4 = ipv4.ipv4(version=prt.version,
                                                header_length=prt.header_length,
                                                tos=prt.tos,
                                                total_length=prt.total_length - shift_amount, # adjust 
                                                identification=prt.identification,
                                                flags=prt.flags,
                                                offset=prt.offset,
                                                ttl=prt.ttl,
                                                proto=prt.proto,
                                                csum=0,
                                                src=prt.src,
                                                dst=prt.dst,
                                                option=prt.option
                                                )
                                new_pkt.add_protocol(ip4)
                            elif isinstance(prt, tcp.tcp):
                                # change tcp header
                                t = tcp.tcp(src_port = prt.src_port,
                                            dst_port = prt.dst_port,
                                            seq = prt.seq + shift_amount, #shift 
                                            # seq = proto.seq,
                                            ack = prt.ack, 
                                            offset = 0,
                                            bits = prt.bits,
                                            window_size = prt.window_size,
                                            csum = 0,
                                            urgent = prt.urgent,
                                            option = prt.option)
                                new_pkt.add_protocol(t)
                            elif isinstance(prt, str):
                                payload = prt
                                # TODO: check if payload has at least shift_amount bytes ...
                                new_pkt.add_protocol(payload[shift_amount:])
                                # new_pkt.add_protocol(payload)
                            # ethernet, ipv4, ...
                            else:
                                new_pkt.add_protocol(prt)
                        new_pkt.serialize()
                        out.data = new_pkt.data

                        # update counter for number of shifted packets on this flow 
                        shift_rule.shifted_flow(pkt_ipv4.src, pkt_ipv4.dst, tcp_header.dst_port, tcp_header.src_port)
                        # for rule in self.TCP_Shifting_Rules:
                        #     print(rule)
            datapath.send_msg(out)
        else:
            datapath.send_msg(out)
