./start_R1_controller_tcp_monitor.sh > controller_logs/R1_controller_logs.log 2> controller_logs/R1_controller_logs.log  &
echo "Instantiating controller for R1"
pid[0]=$!

./start_R3_controller.sh > controller_logs/R3_controller_logs.log 2> controller_logs/R3_controller_logs.log &
echo "Instantiating controller for R3"
pid[1]=$!

./start_R8_sentinel.sh > controller_logs/R8_controller_logs.log 2> controller_logs/R8_controller_logs.log &
echo "Instantiating controller for R8"
pid[2]=$!

trap "kill ${pid[0]} ${pid[1]} ${pid[2]}; exit 1" INT
wait